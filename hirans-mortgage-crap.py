back_end_ratio = .36

mortgage = False
obligations = False
income = False

def get_mortgage():
  global mortgage
  while not mortgage:
    try:
      mortgage = int(input("Input your monthly mortgage or rent payment: $"))
    except ValueError: 
      continue

def get_obligations():
  global obligations
  while not obligations:
    try:
      obligations = int(input("Input non-mortgage / rent monthly obligations: $"))
    except ValueError: 
      continue

def get_income():
  global income
  while not income:
    try:
      income = int(input("Input your monthly paycheck: $"))
    except ValueError: 
      continue
    
parameters = (get_mortgage, get_obligations, get_income)

for func in parameters:
  func()
result = (mortgage + obligations) / income
print("Given Mortgage: $" + str(mortgage) + '\n' +
      "Given Obligations: $" + str(obligations) + '\n' +
      "Given Income: $" + str(income) + '\n' + 
      "Back End Ratio: " + str(back_end_ratio) + '\n' +
      "Perentage of income avialable after obligations and housing: " + str(result))
if(result < back_end_ratio):
  print("Congrats you are currently in a financially secure status")
else:
  print("Damn yo get your life right")
